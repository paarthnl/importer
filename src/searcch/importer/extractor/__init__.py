
import six
from future.utils import iteritems
import abc

from searcch.importer.db.model import (
    Extractor, License)

@six.add_metaclass(abc.ABCMeta)
class BaseExtractor(object):
    """An abstract base class that any Extractor must subclass."""
    name = None
    version = None

    def __init__(self,config,session,**kwargs):
        self._config = config
        self._session = session

    @property
    def config(self):
        return self._config

    @property
    def session(self):
        return self._session

    @abc.abstractmethod
    def extract(self):
        """Extracts all possible metadata from an artifact import session; return value ignored."""

    def get_license_object(self,short_name):
        license = self.session.session.query(License).\
            filter(License.short_name == short_name).\
            first()
        if not license:
            license = License(short_name=short_name)
        return license

from .basic import BasicFileExtractor
from .git import GitExtractor
from .top_keywords import TopKeywordsExtractor
from .yake_ngram_keywords import YakeNGramKeywordsExtractor
from .semantic_scholar import SemanticScholarExtractor
from .markdown_extractor import MarkdownExtractor
from .pke_keywords import PKEExtractor
from .license import LicenseExtractor

__extractors__ = dict()
__extractors__[BasicFileExtractor.name] = BasicFileExtractor
__extractors__[GitExtractor.name] = GitExtractor
__extractors__[TopKeywordsExtractor.name] = TopKeywordsExtractor
__extractors__[YakeNGramKeywordsExtractor.name] = YakeNGramKeywordsExtractor
__extractors__[SemanticScholarExtractor.name] = SemanticScholarExtractor
__extractors__[MarkdownExtractor.name] = MarkdownExtractor
__extractors__[LicenseExtractor.name] = LicenseExtractor
__extractors__[PKEExtractor.name] = PKEExtractor

def get_extractor_names():
    return __extractors__.keys()

def get_extractors(config,session,**kwargs):
    return [cls(config,session,**kwargs) for cls in __extractors__.values()]

def get_extractor(name,config,session,**kwargs):
    if name:
        if not name in __extractors__:
            raise NotImplementedError("no such extractor '%s'" % (str(name),))
        return __extractors__[name]()
    else:
        for name in __extractors__:
            try:
                cls = __extractors__[name]
                return cls(config,session)
            except Exception:
                pass
        return None
