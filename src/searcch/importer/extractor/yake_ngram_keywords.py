
import six
import logging
import sys
import os.path
import os
import requests
import json

import yake

from searcch.importer.extractor import BaseExtractor
from searcch.importer.db.model import (
    ArtifactMetadata, ArtifactTag )
from searcch.importer.util.pdf import pdf_file_to_text

LOG = logging.getLogger(__name__)

class YakeNGramKeywordsExtractor(BaseExtractor):
    """Uses the Yet Another Keyword Extractor to extract keywords from text files."""
    name = "yake_ngram_keywords"
    version = "0.1"

    def __init__(self,config,session,**kwargs):
        self._config = config
        self._session = session

    def extract(self):
        """Extracts keywords from retrieved ArtifactFiles with mime type application/pdf or text and saves the top N as ArtifactMetadata."""

        LOG.debug("scanning for top ngram keywords in artifact %s" % (
            self.session.artifact.url,))

        tags = {}
        
        for rf in self.session.retrieved_files:
            # If this is not a regular file, skip it.
            if os.path.islink(rf.path) or not os.path.isfile(rf.path):
                continue
            # If this is not a PDF, skip it:
            if rf.mime_type not in ("application/pdf","text","txt","application/text","",None):
                continue

            LOG.debug("scanning for top keywords in PDF retrieved file %s" % (
                rf.artifact_file.url),)

            text = None
            if rf.mime_type == "application/pdf":
                text = pdf_file_to_text(rf.path)
            else:
                with open(rf.path,"r") as f:
                    text = f.read()

            ext = yake.KeywordExtractor()
            top_keywords = ext.extract_keywords(text)
            if not len(top_keywords):
                LOG.debug("no top ngram keywords found in retrieved file %s" % (
                    rf.artifact_file.url),)
                continue

            for (kw,rank) in top_keywords:
                # Don't add duplicate tags.
                if kw in tags:
                    continue
                self.session.artifact.tags.append(
                    ArtifactTag(tag=kw,source="yake_ngram_keywords"))
                tags[kw] = rank
            top_keywords = json.dumps(top_keywords, separators=(',', ':'))
            self.session.artifact.meta.append(
                ArtifactMetadata(
                    name="top_ngram_keywords",value=top_keywords,source="yake_ngram_keywords",type="json"))
