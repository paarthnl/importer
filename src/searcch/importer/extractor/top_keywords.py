
import six
import logging
import sys
import os.path
import os
import requests
import json

from nltk import download
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from collections import Counter

from searcch.importer.extractor import BaseExtractor
from searcch.importer.db.model import (
    ArtifactMetadata, ArtifactTag )
from searcch.importer.util.pdf import pdf_file_to_text

LOG = logging.getLogger(__name__)

def requests_download(url,destpath,retries=2,interval=2):
    while retries >= 0:
        try:
            session = requests.session()
            response = session.get(url,stream=True,timeout=4)
            response.raise_for_status()
            fd = open(destpath,'wb')
            for buf in response.iter_content(4096):
                fd.write(buf)
            fd.close()
            return response
        except:
            if retries == 0:
                six.reraise(*sys.exc_info())
        retries -= 1
    return None

class TopKeywordsExtractor(BaseExtractor):
    """A simple extractor that grabs the top N keywords from an ArtifactFile that is a PDF."""
    name = "top_keywords"
    version = "0.1"

    def __init__(self,config,session,**kwargs):
        self._config = config
        self._session = session
        self._common_words = []
        self._stop_words = set()
        self._loaded = False

    def load(self):
        if self._loaded:
            return
        LOG.debug("loading nltk and token data")
        tmpdir = self.config["DEFAULT"]["tmpdir"]
        download("stopwords") #,download_dir=tmpdir)
        self._stop_words = set(stopwords.words("english"))
        download("punkt") #,download_dir=tmpdir)
        cw_url = "https://github.com/first20hours/google-10000-english/raw/master/google-10000-english-no-swears.txt"
        top_file = os.path.join(tmpdir,"google-10000-english-no-swears.txt")
        if not os.path.exists(top_file):
            requests_download(cw_url,top_file)
        with open(top_file,"rb") as f:
            self._common_words = set(f.read().decode().split())
        self._loaded = True

    def clean(self,line):
        # tokenize, ignore non-alpha's and stop or common words
        stripped = [w for w in word_tokenize(line) if w.isalpha() and not (
            w in self._stop_words or w in self._common_words)]
        return(list(filter(None, stripped)))

    def extract(self):
        """Extracts keywords from retrieved ArtifactFiles with mime type application/pdf or text and saves the top N as ArtifactMetadata."""

        LOG.debug("scanning for top keywords in artifact %s" % (
            self.session.artifact.url,))

        tags = {}
        
        for rf in self.session.retrieved_files:
            # If this is not a regular file, skip it.
            if os.path.islink(rf.path) or not os.path.isfile(rf.path):
                continue
            # If this is not a PDF, skip it:
            if rf.mime_type not in ("application/pdf","text","txt","application/text","",None):
                continue

            LOG.debug("scanning for top keywords in PDF retrieved file %s" % (
                rf.artifact_file.url),)

            self.load()

            text = None
            if rf.mime_type == "application/pdf":
                text = pdf_file_to_text(rf.path)
            else:
                with open(rf.path,"r") as f:
                    text = f.read()

            counts = Counter(self.clean(text.lower()))
            if not len(counts):
                LOG.debug("no top keywords found in retrieved file %s" % (
                    rf.artifact_file.url),)
                continue

            top = counts.most_common(10)
            for (kw,count) in top:
                # Don't add duplicate tags.
                if kw in tags:
                    continue
                self.session.artifact.tags.append(
                    ArtifactTag(tag=kw,source="top_keywords"))
                tags[kw] = count

            top_keywords = json.dumps(top, separators=(',', ':'))
            self.session.artifact.meta.append(
                ArtifactMetadata(
                    name="top_keywords",value=top_keywords,source="top_keywords",type="json"))
