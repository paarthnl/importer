import six
import logging
import sys
import os.path
import os
import requests
import json
import spacy
import nltk
import pke

# "patch" pke to work with spacy 3.x
pke.base.lang_stopwords['en_core_web_sm'] = 'english'
pke.base.lang_stem.update({'en_core_web_sm': 'porter'})

from searcch.importer.extractor import BaseExtractor
from searcch.importer.db.model import (
    ArtifactMetadata, ArtifactTag )
from searcch.importer.util.pdf import pdf_file_to_text

LOG = logging.getLogger(__name__)

class PKEExtractor(BaseExtractor):
    """Uses the PKE toolkit to extract keywords from text files."""
    name = "pke_keywords"
    version = "0.1"

    def __init__(self,config,session,**kwargs):
        self._config = config
        self._session = session
        try:
            sp = spacy.load("en_core_web_sm")
        except:
            spacy.cli.download("en_core_web_sm")
            sp = spacy.load("en_core_web_sm")
        nltk.download('stopwords')

    def extract(self):
        """Extracts keywords from retrieved ArtifactFiles with mime type application/pdf or text and saves the top N as ArtifactMetadata."""

        LOG.debug("scanning for top ngram keywords in artifact %s" % (
            self.session.artifact.url,))

        tags = {}

        for rf in self.session.retrieved_files:
            # If this is not a regular file, skip it.
            if os.path.islink(rf.path) or not os.path.isfile(rf.path):
                continue
            # If this is not a PDF, skip it:
            if rf.mime_type not in ("application/pdf","text","txt","application/text","",None):
                continue

            LOG.debug("scanning for top keywords via PKE in retrieved file %s" % (
                rf.artifact_file.url),)

            text = None
            if rf.mime_type == "application/pdf":
                text = pdf_file_to_text(rf.path)
            else:
                with open(rf.path,"r") as f:
                    text = f.read()

            keywords = []
            extractor = pke.unsupervised.TopicRank()
            extractor.load_document(text, language="en_core_web_sm")
            extractor.candidate_selection()
            extractor.candidate_weighting()
            keyphrases = extractor.get_n_best(n=10)
            maxi=-float('inf')
            for i in keyphrases:
                maxi = max(maxi,i[1])
            normalized = []
            for i in keyphrases:
                normalized.append((i[0],i[1]/maxi))
            keywords.extend(normalized)
            extractor = pke.unsupervised.YAKE()
            extractor.load_document(text, language="en_core_web_sm")
            extractor.candidate_selection()
            extractor.candidate_weighting()
            keyphrases = extractor.get_n_best(n=10)
            maxi=-float('inf')
            for i in keyphrases:
                maxi = max(maxi,i[1])
            normalized = []
            for i in keyphrases:
                normalized.append((i[0],i[1]/maxi))
            keywords.extend(normalized)
            extractor = pke.unsupervised.MultipartiteRank()
            extractor.load_document(text, language="en_core_web_sm")
            extractor.candidate_selection()
            extractor.candidate_weighting()
            keyphrases = extractor.get_n_best(n=10)
            maxi=-float('inf')
            for i in keyphrases:
                maxi = max(maxi,i[1])
            normalized = []
            for i in keyphrases:
                normalized.append((i[0],i[1]/maxi))
            keywords.extend(normalized)

            keywords.sort(key = lambda x:x[1],reverse=True)
            seen=set()
            filtered_keywords = []
            for i in keywords:
                if(i[0] not in seen):
                    filtered_keywords.append(i)
                    seen.add(i[0])

            if not len(filtered_keywords):
                LOG.debug("no pke keywords found in retrieved file %s" % (
                    rf.artifact_file.url),)
                continue

            # Add in ArtifacTags, but don't duplicate.
            for (kw,rank) in filtered_keywords:
                if kw in tags:
                    continue
                self.session.artifact.tags.append(
                    ArtifactTag(tag=kw,source="pke_keywords"))
                tags[kw] = rank

            pke_keywords = json.dumps(filtered_keywords, separators=(',', ':'))
            self.session.artifact.meta.append(
                ArtifactMetadata(
                    name="pke_keywords",value=pke_keywords,source="pke_keywords",type="json"))
