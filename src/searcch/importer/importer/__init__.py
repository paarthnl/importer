
import six
from future.utils import iteritems
import abc
import os
import os.path
import shutil
import requests
import sys
import logging

from searcch.importer.db.model import (
    Importer,Person,User,License)
from searcch.importer.util.retrieve import Retriever
from searcch.importer.extractor import get_extractors

LOG = logging.getLogger(__name__)

@six.add_metaclass(abc.ABCMeta)
class BaseImporter(object):
    """An abstract base class that any Importer must subclass."""
    name = None
    version = None

    def __init__(self,config,session):
        self._config = config
        self._session = session

    @property
    def importer_object(self):
        imp = self._session.query(Importer).\
            filter(Importer.name == self.__class__.name).\
            filter(Importer.version == self.__class__.version).\
            first()
        if not imp:
            imp = Importer(name=self.__class__.name,
                           version=self.__class__.version)
        return imp

    @property
    def owner_object(self):
        person = self._session.query(Person).\
            filter(Person.name == self.config["DEFAULT"]["user_name"]).\
            filter(Person.email == self.config["DEFAULT"]["user_email"]).\
            first()
        if not person:
            person = Person(email=self.config["DEFAULT"]["user_email"],
                            name=self.config["DEFAULT"]["user_name"])
            owner = User(person=person)
        else:
            owner = self._session.query(User).\
                filter(User.person == person).\
                first()
            if not owner:
                owner = User(person=person)
        return owner

    def get_license_object(self,short_name):
        license = self._session.query(License).\
            filter(License.short_name == short_name).\
            first()
        if not license:
            license = License(short_name=short_name)
        return license

    @property
    def config(self):
        return self._config

    @property
    def session(self):
        return self._session

    @abc.abstractclassmethod
    def can_import(cls,url):
        """Checks to see if this URL can be imported by this Importer."""

    @abc.abstractmethod
    def import_artifact(self,candidate):
        """Imports an artifact from the given CandidateArtifact and returns an Artifact."""

from .github import GithubImporter
from .zenodo import ZenodoImporter
from .acm_dl import AcmDigitalLibraryImporter
from .ieeexplore import IeeeXploreImporter
from .usenix import USENIXImporter
from .gitrepo import GitRepoImporter
from .arxiv import ArxivImporter
from .paperswithcode import PapersWithCodeImporter


__importers__ = dict()
__importers__[GithubImporter.name] = GithubImporter
__importers__[ZenodoImporter.name] = ZenodoImporter
__importers__[AcmDigitalLibraryImporter.name] = AcmDigitalLibraryImporter
__importers__[IeeeXploreImporter.name] = IeeeXploreImporter
__importers__[USENIXImporter.name] = USENIXImporter
__importers__[PapersWithCodeImporter.name] = PapersWithCodeImporter
__importers__[ArxivImporter.name] = ArxivImporter
__importers__[GitRepoImporter.name] = GitRepoImporter


def get_importer_names():
    return __importers__.keys()

def validate_url(url,retries=0,interval=None):
    errors = []
    while retries >= 0:
        retries -= 1
        try:
            requests.head(url)
        except requests.exceptions.RequestException:
            if retries < 0:
                six.reraise(*sys.exc_info())
        except:
            LOG.exception(sys.exc_info()[1])
            if retries < 0:
                six.reraise(*sys.exc_info())

def get_importer(url,config,session,name=None,retries=0,interval=None):
    validate_url(url,retries=retries,interval=interval)
    if name:
        if not name in __importers__:
            raise NotImplementedError("no such importer '%s'" % (str(name),))
        return __importers__[name]()
    else:
        for name in __importers__:
            try:
                cls = __importers__[name]
                i = cls(config,session)
                if i.can_import(url):
                    return i
            except Exception:
                pass
        return None

class ImportSession(object):
    """A helper class that contains and manages state (e.g. temporary filesystem content) accumulated during an import session -- retrieval, unpacking, extraction(s)."""

    def __init__(self,config,session,artifact,sessionid=None):
        self._config = config
        self._session = session
        self._artifact = artifact
        self._retrieved_files = []
        self._sessionid_journal_path = os.path.join(
            config["DEFAULT"]["tmpdir"],".last_sessionid")
        self._sessionid = sessionid or self._get_next_sessionid()
        self._session_destdir = os.path.join(
            config["DEFAULT"]["tmpdir"],str(self._sessionid))
        self._artifact_destdir = os.path.join(
            config["DEFAULT"]["tmpdir"],str(self._sessionid))

    def _get_next_sessionid(self):
        n = 0
        if os.path.exists(self._sessionid_journal_path):
            f = open(self._sessionid_journal_path,'r')
            try:
                n = int(f.read()) + 1
            except:
                pass
            f.close()
        sd = os.path.join(self.config["DEFAULT"]["tmpdir"],str(n))
        while os.path.exists(sd):
            n += 1
            sd = os.path.join(self.config["DEFAULT"]["tmpdir"],str(n))
        os.makedirs(sd)
        f = open(self._sessionid_journal_path,'w')
        f.write(str(n))
        f.close()
        return n

    @property
    def config(self):
        return self._config

    @property
    def session(self):
        return self._session

    @property
    def artifact(self):
        return self._artifact

    @property
    def retrieved_files(self):
        return self._retrieved_files

    def retrieve_all(self):
        r = Retriever(self.config)
        i = 0
        for af in self.artifact.files:
            destdir = os.path.join(self._artifact_destdir,str(i))
            rf = r.retrieve(af,self.artifact,destdir=destdir)
            if rf:
                self._retrieved_files.append(rf)
                LOG.debug("retrieved file: %r" % (rf,))
            i += 1

    def extract_all(self,skip=[]):
        """Runs all extractors (except those identified in the `skip` arg) across the artifact and its content."""
        for ext in get_extractors(self.config,self):
            if skip and ext.name in skip:
                continue
            ext.extract()

    def remove_retrieved_files(self):
        """Removes all temporary filesystem content for each retrieved file."""
        for rf in self.retrieved_files:
            #os.removedirs(rf.destdir)
            if os.path.exists(rf.destdir):
                shutil.rmtree(rf.destdir)

    def remove_all(self):
        """Removes all temporary filesystem content."""
        self.remove_retrieved_files()
        #os.removedirs(self._artifact_destdir)
        if os.path.exists(self._artifact_destdir):
            shutil.rmtree(self._artifact_destdir)
        if os.path.exists(self._session_destdir):
            shutil.rmtree(self._session_destdir)

    def finalize(self):
        """Automatically fills in as many unset fields in the main Artifact object as possible, as necessary to be in compliance with the schema; although there are no good choices."""
        if not self.artifact.title:
            if self.artifact.name:
                self.artifact.title = self.artifact.name
            else:
                self.artifact.title = self.artifact.url
