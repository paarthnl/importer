import sys
import six
import os
import logging
import dateutil.parser
import datetime
import urllib
import requests
from future.utils import raise_from
from urllib.parse import urlparse
import bs4
import bibtexparser

from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact, ArtifactFile, ArtifactMetadata, ArtifactRelease, User, Person,
    Importer, Affiliation, ArtifactAffiliation, ArtifactTag, Organization,
    Badge, ArtifactBadge)

LOG = logging.getLogger(__name__)

class USENIXImporter(BaseImporter):
    """Provides an USENIX DOI Importer."""

    name = "usenix"
    version = "0.1"

    @classmethod
    def _extract_record_id(self, url):
        try:
            session = requests.session()
            res = requests.get(url)
            urlobj = urlparse(res.url)
            if urlobj.netloc.endswith("usenix.org") \
              and urlobj.path.startswith("/conference/") and "/presentation/" in urlobj.path:
                return (urlobj.path[len("/conference/"):].rstrip("/"), res.url, session)
        except BaseException:
            return None

    @classmethod
    def can_import(cls, url):
        """Checks to see if this URL is a doi.org or usenix.org/conference URL."""
        if cls._extract_record_id(url):
            return True
        return False

    def import_artifact(self, candidate, parent=None):
        """Imports an artifact from USENIX and returns an Artifact, or throws an error."""
        url = candidate.url
        LOG.debug("importing '%s' from USENIX " % (url,))
        page = requests.get(url)
        soup = bs4.BeautifulSoup(page.content, 'html.parser')

        title = soup.find('h1',id='page-title')
        if not title:
            LOG.warn("no title in data")
        else:
            title = title.text
        
        abstract=""
        abst = soup.find('div',class_='field-name-field-paper-description')
        if not abst:
            LOG.warn("no abstract in data")
        else:
            abstract = abst.find('div' , class_='field-item odd').text

        bib_text = soup.find('div', class_='bibtex-text-entry').text

        bib_dict={}
        if not bib_text:
            LOG.warn("no bibtext in metadata")
        else:
            bib_database = bibtexparser.loads(bib_text)
            bib_dict = bib_database.entries[0]

        affiliations = []
        org_dict = {}

        try:
            author_list = soup.find('div', class_='field-name-field-paper-people-text').\
              find('div' , class_='field-item odd').\
              find('p').text.split(';')
            for i in author_list:
                author = i.split(',')
                (org_name,country) = (None,None)
                offset = None
                if len(author) == 3:
                    (org_name,country) = (author[1].strip(),author[2].strip())
                    offset = 2
                else:
                    org_name = author[-1].strip()
                    offset = 1
                # Sometimes USENIX stuffs extra stuff after the author list,
                # like distinguished paper winner.
                newline_idx = org_name.find("\n")
                if newline_idx > 0:
                    org_name = org_name[:newline_idx]
                if org_name in org_dict:
                    org = org_dict[org_name]
                else:
                    org = Organization(name=org_name, type="Institution",
                                       country=country)
                    org_dict[org_name] = org
                    for a in range(len(author) - offset):
                        if author[a].startswith(" and "):
                            names = author[a].split()
                            person = Person(name=names[1].strip())
                            affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person,org=org),roles="Author"))
                        else:
                            person = Person(name=author[a].strip())
                            affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person,org=org),roles="Author"))
        except:
            LOG.warning("failed to parse author list")
            LOG.exception(sys.exc_info()[1])
        if not affiliations:
            if('author' in bib_dict):
                authors = bib_dict['author'].split('and')
                for a in authors:
                    affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=Person(name=a.strip())),roles="Author"))
            else:
                LOG.warn("no authors in metadata")

        meta_data = {}
        if 'isbn' in bib_dict:
            meta_data['ISBN'] = bib_dict['isbn']
        if 'year' in bib_dict:
            meta_data['year'] = bib_dict['year']
        if 'publisher' in bib_dict:
            meta_data['publisher'] = bib_dict['publisher']
        if 'pages' in bib_dict:
            meta_data['page'] = bib_dict['pages']

        verbatim_metadata_names = [
            "collection-title", "container-title", "call-number", "event-place",
            "ISBN", "number-of-pages", "page", "publisher", "publisher-place", 'year' ]
        metadata = list()
        for vmn in verbatim_metadata_names:
            if not vmn in meta_data:
                continue
            metadata.append(ArtifactMetadata(name=vmn, value=str(meta_data[vmn])))

        artifact_badges = []
        evaluation_badge = soup.find('div', class_='field-name-field-artifact-evaluated')
        if evaluation_badge:
            evaluation_badge_link = evaluation_badge.find('img')['src']
            metadata.append(ArtifactMetadata(
                        name="badge",value=str(evaluation_badge_link), type="text/json",
                        source="usenix"))
            badge_object = self.session.query(Badge).\
              filter(Badge.image_url == evaluation_badge_link).\
              filter(Badge.verified == True).first()
            if badge_object:
                artifact_badges.append(ArtifactBadge(badge=badge_object))

        presentation_video= soup.find('div', class_='embedded-video')
        if presentation_video:
            presentation_video_link = presentation_video.find('iframe')['src']
            parsed_url = urlparse(presentation_video_link)
            if not parsed_url.scheme:
                presentation_video_link = "https:" + presentation_video_link
            metadata.append(ArtifactMetadata(
                        name="presentation_video", value=str(presentation_video_link), type="text/json",
                        source="usenix"))

        artifact_files = []
        presentation_pdf = soup.find('div', class_='field-name-field-presentation-pdf')
        if presentation_pdf:
            presentation_pdf_link = presentation_pdf.find('a')['href']
            path = urlparse(presentation_pdf_link).path
            ext = os.path.splitext(path)
            typ = ext[1][1:]
            name = ext[0].split('/')[-1]+ext[1]
            artifact_files.append(ArtifactFile(
                        name=name, url=presentation_pdf_link, filetype="application/"+typ))
   
        presentation_slides= soup.find('div', class_ = 'field-name-field-paper-slides-file')
        if presentation_slides:
            presentation_slides_link = presentation_slides.find('a')['href']
            path = urlparse(presentation_slides_link).path
            ext = os.path.splitext(path)
            typ = ext[1][1:]
            name = ext[0].split('/')[-1]+ext[1]
            artifact_files.append(ArtifactFile(
                        name=name, url=presentation_slides_link, filetype="application/"+typ))

        return Artifact(
            type="publication",url=url,title=title,description=abstract,
            name=title,ctime=datetime.datetime.now(),ext_id=url,
            owner=self.owner_object,importer=self.importer_object,
            tags=[],meta=metadata,files=artifact_files,affiliations=affiliations,
            badges=artifact_badges,parent=parent)
