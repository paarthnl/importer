
import sys
import six
import logging
import time
import dateutil.parser
import datetime
import requests
from future.utils import raise_from
from urllib.parse import urlparse
import rispy

from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact,ArtifactFile,ArtifactMetadata,ArtifactRelease,User,Person,
    Importer,Affiliation,ArtifactAffiliation,ArtifactTag )

LOG = logging.getLogger(__name__)

class IeeeXploreImporter(BaseImporter):
    """Provides an IeeeXplore DOI Importer."""

    name = "ieeexplore"
    version = "0.1"

    @classmethod
    def _extract_record_id(self,url):
        try:
            session = requests.session()
            res = requests.get(url)
            urlobj = urlparse(res.url)
            if urlobj.netloc == "ieeexplore.ieee.org" \
              and urlobj.path.startswith("/document/"):
                return (urlobj.path[len("/document/"):].rstrip("/"),res.url,session)
        except BaseException:
            return None

    @classmethod
    def can_import(cls,url):
        """Checks to see if this URL is a doi.org or ieeexplore.ieee.org/document URL."""
        if cls._extract_record_id(url):
            return True
        return False

    def import_artifact(self,candidate,parent=None):
        """Imports an artifact from IEEE Xplore and returns an Artifact, or throws an error."""
        url = candidate.url
        LOG.debug("importing '%s' from IEEE Xplore" % (url,))
        (record_id,newurl,session) = self.__class__._extract_record_id(url)
        res = session.get(
            "https://ieeexplore.ieee.org/rest/search/citation/format?recordIds=%s&download-format=download-ris" % (record_id),
            headers={"Referer":newurl})
        res.raise_for_status()
        LOG.debug("record: %r",res.json())
        j = rispy.loads(res.json()["data"])[0]

        title = name = j.get("title")
        if not title:
            raise Exception("no title in RIS metadata")
        doi = j.get("doi","")
        if not doi:
            raise Exception("no DOI in RIS metadata")
        description = j.get("abstract")
        if not description:
            LOG.warning("%s (%s) missing abstract",newurl,url)

        authors = []
        for a in j.get("authors",[]):
            authors.append(Person(name=a))
        affiliations = []
        for person in authors:
            LOG.debug("adding author %r",person)
            affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person),roles="Author"))

        tags = list()
        seentags = dict()
        for kw in j.get("keywords",[]):
            if kw in seentags:
                continue
            tags.append(ArtifactTag(tag=kw,source="ieeexplore"))
            seentags[kw] = True

        metadata = list()
        verbatim_metadata_names = [
            "doi","start_page","end_page","secondary_title","type_of_reference",
            "year","journal_name","number","issn","publication_year" ]
        for vmn in verbatim_metadata_names:
            if not vmn in j:
                continue
            metadata.append(ArtifactMetadata(name=vmn,value=str(j[vmn])))

        record_url = "https://doi.org/%s" % (doi,)

        files = []
        LOG.debug("attempting PDF fetch via %r from IEEE Xplore",url)
        try:
            res = session.get(url)
            res.raise_for_status()
            href = "https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=" + record_id
            res = session.head(href)
            if res.status_code == requests.codes.ok:
                filename = record_id + ".pdf"
                files.append(
                    ArtifactFile(url=href,name=filename,filetype="application/pdf"))
                LOG.debug("found PDF file at %r",href)
            else:
                LOG.warning("failed to HEAD the pdf link (%r,%r); cannot download",
                            href,res.status_code)
        except:
            LOG.warning("failed PDF fetch via %r from IEEE Xplore",url)

        return Artifact(
            type="publication",url=record_url,title=title,description=description,
            name=name,ctime=datetime.datetime.now(),ext_id=record_id,
            owner=self.owner_object,importer=self.importer_object,
            tags=tags,meta=metadata,affiliations=affiliations,files=files,parent=parent)
